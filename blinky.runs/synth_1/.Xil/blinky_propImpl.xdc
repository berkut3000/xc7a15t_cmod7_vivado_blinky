set_property SRC_FILE_INFO {cfile:C:/Users/aguilan1/Documents/CIDESIFPGA/digilent-xdc-master/digilent-xdc-master/Cmod-A7-Master.xdc rfile:../../../../../digilent-xdc-master/digilent-xdc-master/Cmod-A7-Master.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN L17   IOSTANDARD LVCMOS33 } [get_ports { clk }]; #IO_L12P_T1_MRCC_14 Sch=gclk
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN A17   IOSTANDARD LVCMOS33 } [get_ports { led }]; #IO_L12N_T1_MRCC_16 Sch=led[1]
